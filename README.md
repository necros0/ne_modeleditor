## Disclaimer ##

If you are thinking this code is terrible, you would be right.  I would recommend you not use this project as a learning tool.  You will suck if you do so.
And yes, I know the GL draw commands are slow and deprecated and I should be using VBOs, but this is just simpler, and I don't care right now.

# ne_modelEditor v0.04 #

This is a very limited model editor that lets you do things that are missing from other editors, but doesn't let you do most of what you can do in other editors.

## What you can do ##

* Import a .pcx skin (this must already have the palette properly set by QME)
* Modify UVs
* Convert between MDL and OBJ formats
* Batch rename frames in a model by using a text file with frame names, one per line

## What you can't do ##

* You can't edit vertex positions in frames.
* You can't edit skins